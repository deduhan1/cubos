﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerKeyboard : MonoBehaviour
{
    private PlayerInput _input;

    public event UnityAction<Vector2> Moving;

    private void Awake()
    {
        _input = new PlayerInput();
        _input.Enable();

        _input.Cube.Move.started += ctx => ButtonPressed();
    }

    private void ButtonPressed()
    {
        Vector2 direction = _input.Cube.Move.ReadValue<Vector2>();

        if (direction.x == direction.y)
            direction.y = 0;

        Moving.Invoke(direction);
    }
}
