﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Desk : MonoBehaviour
{
    [SerializeField] private int _weight = 10;
    [SerializeField] private int _height = 10;
    [SerializeField] private Platform _template;
    [SerializeField] private Cube _cube;

    public Platform[,] Platforms { get; private set; }

    private void Awake()
    {
        Platforms = new Platform[_weight, _height];
    }

    private void OnEnable()
    {
        _cube.ReachedPlatform += OnReachedPlatform;
    }

    private void OnDisable()
    {
        _cube.ReachedPlatform -= OnReachedPlatform;
    }

    private void Start()
    {
        CreateDesk();
        ChargePlatform();
    }

    public bool Access(Vector2 position)
    {
        if (position.x >= 0 && position.y >= 0 && position.x < Platforms.GetLength(0) && position.y < Platforms.GetLength(1))
            return true;

        return false;
    }

    public Platform GetPlatform(Vector2 position)
    {
        return Platforms[(int)position.x, (int)position.y];
    }

    private void ChargePlatform()
    {
        List<Platform> emptyPlatforms = new List<Platform>();

        foreach (Platform platform in Platforms)
        {
            if(platform.Power is StandartPower && !platform.IsLock)
                emptyPlatforms.Add(platform);
        }

        IPower power = new DoubleMovePower();
        Platform randomPlatform = emptyPlatforms[Random.Range(0, emptyPlatforms.Count)];
        randomPlatform.SetPower(power);
    }

    private void CreateDesk()
    {
        for (int i = 0; i < _weight; i++)
        {
            for (int j = 0; j < _height; j++)
            {
                Platform platform = Instantiate(_template, new Vector3(i, 0, j), Quaternion.identity);
                platform.SetPower(new StandartPower());
                platform.Recharging += ChargePlatform;
                Platforms[i, j] = platform;
            }
        }
    }

    private void OnReachedPlatform(Platform platform)
    {
        platform.Lock();

        if (!(platform.Power is StandartPower))
            ChargePlatform();
    }
}
