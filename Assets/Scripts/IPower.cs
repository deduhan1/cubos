﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPower
{
    void Init();

    void Move(Vector2 direction, Desk desk, Cube cube);

    Platform TargetPlatform(Vector2 direction, Desk desk, Cube cube);

    Color GetColor();

    int GetMoves();
}
