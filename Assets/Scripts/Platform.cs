﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Platform : MonoBehaviour
{
    [SerializeField] private GameObject _box;

    private MeshRenderer _meshRenderer;

    public event UnityAction Recharging;

    public IPower Power { get; private set; }
    public bool IsLock { get; private set; }

    private void Awake()
    {
        _meshRenderer = _box.GetComponent<MeshRenderer>();
    }

    public void SetStandartPower()
    {
        Power = new StandartPower();
        Power.Init();
        ChangeColor(Power.GetColor());
        Recharging?.Invoke();
    }

    public void SetPower(IPower power)
    {
        Power = power;
        Power.Init();
        ChangeColor(Power.GetColor());
    }

    public void Lock()
    {
        IsLock = true;
        ChangeColor(Color.blue);
    }

    public void Unlock()
    {
        IsLock = true;
        SetStandartPower();
        ChangeColor(Color.blue);
    }

    public void ChangeColor(Color color)
    {
        _meshRenderer.material.color = color;
    }
}
