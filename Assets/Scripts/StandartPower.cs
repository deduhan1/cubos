﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandartPower : IPower
{
    private Color _color;
    private int _moves;

    public Color GetColor()
    {
        return _color;
    }

    public int GetMoves()
    {
        return _moves;
    }

    public void Init()
    {
        _color = Color.grey;
        _moves = 1;
    }

    public void Move(Vector2 direction, Desk desk, Cube cube)
    {
        Platform platform = TargetPlatform(direction, desk, cube);

        if (platform == null || platform.IsLock)
            return;

        Vector2 platformPosition = cube.Position + direction;
        cube.ChangePosition(platformPosition);
        _moves--;
    }

    public Platform TargetPlatform(Vector2 direction, Desk desk, Cube cube)
    {
        Vector2 platformPosition = cube.Position + direction;

        if (desk.Access(platformPosition))
            return desk.GetPlatform(platformPosition);

        return null;
    }
}
