﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Cube : MonoBehaviour
{
    public enum State
    {
        Idle,
        Move
    }

    [SerializeField] private Desk _desk;
    [SerializeField] private float _moveTime = 0.5f;
    [SerializeField] private PlayerKeyboard _playerKeyboard;

    public event UnityAction<Platform> ReachedPlatform;

    public Vector2 Position { get; private set; }

    private State _currentState;
    private IPower _power;

    public State CurrentState => _currentState;

    private void Awake()
    {
        Position = Vector2.zero;
        transform.position = new Vector3(Position.x, 0.5f, Position.y);
    }

    private void OnEnable()
    {
        _playerKeyboard.Moving += OnMove;
    }

    private void OnDisable()
    {
        _playerKeyboard.Moving -= OnMove;
    }

    private void Start()
    {
        Charge();
        StartCoroutine(IdleCoroutine());
    }

    private void OnMove(Vector2 direction)
    {
        if (CurrentState == State.Move)
            return;

        _power.Move(direction, _desk, this);
    }

    public void ChangePosition(Vector2 newPosition)
    {
        Position = newPosition;
        StartCoroutine(MoveCoroutine());
    }

    private void Charge()
    {
        Platform currentPlatform = _desk.GetPlatform(Position);

        _power = currentPlatform.Power;
        _power.Init();
    }

    private IEnumerator IdleCoroutine()
    {
        _currentState = State.Idle;

        while (_currentState == State.Idle)
        {
            yield return null;
        }
    }

    private IEnumerator MoveCoroutine()
    {
        _currentState = State.Move;

        StartCoroutine(PerformRotatingCoroutine(Position - new Vector2(transform.position.x, transform.position.z), _moveTime));
        StartCoroutine(PerformMovingCoroutine(transform.position, new Vector3(Position.x, 0.5f, Position.y), _moveTime));

        yield return new WaitForSeconds(_moveTime);

        ReachedPlatform?.Invoke(_desk.GetPlatform(Position));
        if (_power.GetMoves() == 0 || !(_desk.GetPlatform(Position).Power is StandartPower))
            Charge();

        StartCoroutine(IdleCoroutine());
    }

    private IEnumerator PerformMovingCoroutine(Vector3 oldPosition, Vector3 newPosition, float time)
    {
        float timeLeft = 0;
        float step = (newPosition - oldPosition).x == 0 ? (newPosition - oldPosition).z : (newPosition - oldPosition).x;
        float radius = Mathf.Sqrt(2 * Mathf.Pow(GetComponent<BoxCollider>().size.x, 2)) * Mathf.Abs(step);
        float startPosition = transform.position.y;

        while (timeLeft < time)
        {
            timeLeft += Time.deltaTime;

            float angel = timeLeft / time < 0.5f ? Mathf.Lerp(0f, 45f, timeLeft / time) : Mathf.Lerp(45f, 0f, timeLeft / time);
            float hight = radius * Mathf.Sin(angel * Mathf.PI / 180f);

            float f = Mathf.Lerp(oldPosition.x, newPosition.x, timeLeft / time);

            transform.position = Vector3.Lerp(oldPosition, newPosition, timeLeft / time);
            transform.position = new Vector3(transform.position.x, startPosition + hight, transform.position.z);

            yield return null;
        }

        transform.position = newPosition;
    }

    private IEnumerator PerformRotatingCoroutine(Vector2 direction, float time)
    {
        float timeLeft = 0;
        float startRotation = direction.y == 0 ? transform.eulerAngles.y : transform.eulerAngles.x;
        float endRotation;

        if (direction.y == 0)
            endRotation = startRotation + direction.x > 0 ? -90f : 90f;
        else
            endRotation = startRotation + direction.y > 0 ? 90f : -90f;

        while (timeLeft < time)
        {
            timeLeft += Time.deltaTime;

            float rotation = Mathf.Lerp(startRotation, endRotation, timeLeft / time) % 90.0f;

            if (direction.y == 0)
                transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, rotation);
            else
                transform.eulerAngles = new Vector3(rotation, transform.eulerAngles.y, transform.eulerAngles.z);

            yield return null;
        }
    }
}
