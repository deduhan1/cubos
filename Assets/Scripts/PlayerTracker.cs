﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTracker : MonoBehaviour
{
    [SerializeField] private Cube _cube;
    [SerializeField] private float _PaddingX = 3f;
    [SerializeField] private float _PaddingZ = 3f;

    private void Start()
    {
        if (_cube == null)
            _cube = FindObjectOfType<Cube>();
    }

    private void Update()
    {
        transform.position = new Vector3(_cube.transform.position.x - _PaddingX, transform.position.y, _cube.transform.position.z - _PaddingZ);
    }
}
